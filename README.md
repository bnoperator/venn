# README #

RStepOperator for creating a VENN diagram. The app can be installed from the PamApp store.


#DOCUMENTATION#
https://pamcloud.pamgene.com/wiki/Wiki.jsp?page=PamApp%20for%20Venn%20Diagram

App users can use the issue tracker here for logging issues.